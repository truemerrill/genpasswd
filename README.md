
genpasswd
=========

`genpasswd` is a simple python-based command line utility to generate random passwords.  Although many such tools already exist, for instance web based services such as [random.org](http://www.random.org/passwords/), `genpasswd` is extremely simple and transparent, which is an advantage for the more paranoid of us out there.

Installation
------------

Installation is straightforward.  Simply change to the `genpasswd` directory and run the setup.py script ... 

```
    python setup.py install
```

`genpasswd` is distributed as a python distutils package.  See the [distutils documentation](http://docs.python.org/2/distutils/) for more information about python packages and distribution.

Usage
-----

`genpasswd` requires two pieces of information to produce a random password: a password length and a character set used to generate the password.  By default, the character set includes mixed-case ASCII letters, digits, and punctuation symbols.  The character set can be controlled using several optional flags, described below.

```
    usage: genpasswd [option] ... [length]
    Options and arguments:
    length         : Length of the password
    --default      : Use default character set to generate password, equivalent to
                     using --mixedcase --numeric and --punctuation options
    --lowercase    : Use lowercase character set
    --uppercase    : Use uppercase character set
    --mixedcase    : Use mixed case character set
    --numeric      : Use numeric character set
    --alphanumeric : Use alphanumeric character set, equivalent to using both 
                     --mixexcase and --numeric options
    --punctuation  : Use punctuation character set
    --help         : Print usage information
```

It is possible to use multiple optional flags to control the character set.  For instance using both `--numeric` and `--lowercase` will use both character sets.  The only required argument is the password length, which should be a positive integer number of character that make up a password.
