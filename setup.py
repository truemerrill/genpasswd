#!/usr/bin/python

from distutils.core import setup

with file("README.md") as f:
    ReadmeContents = f.read()
    f.close()

setup(
    name = "genpasswd",
    version = "0.1",
    description = "A simple random password generator",
    long_description = ReadmeContents,
    author = "True Merrill",
    author_email = "truemerrill@gmail.com",
    url = "https://bitbucket.org/truemerrill/genpasswd",
    scripts = ["src/genpasswd"],
    license = "GPLv3 or later",
    platforms = [
        "Operating System :: POSTIX",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: MacOs :: MacOS X"
        ],
    classifiers = [
        "Environment :: Console",
        "Programming Language :: Python"
        ]
    )
